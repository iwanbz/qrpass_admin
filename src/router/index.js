/* eslint-disable no-unused-vars */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import DefaultLayout from '@/layouts/Default'
import firebase from 'firebase'
import 'firebase/firestore'
Vue.use(VueRouter)

const routes = [
  {
    name: 'root',
    path: '/',
    component: DefaultLayout,
    children: [
      {
        path: '/',
        name: 'home',
        component: Home
      },
      {
        path: '/users',
        name: 'users',
        component: () => import(/* webpackChunkName: "users" */ '@/views/Users.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/new_members',
        name: 'new_members',
        component: () => import(/* webpackChunkName: "new_members" */ '@/views/NewMembers.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/vehicles',
        name: 'vehicles',
        component: () => import(/* webpackChunkName: "vehicles" */ '@/views/Vehicles.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/new_vehicles',
        name: 'new_vehicles',
        component: () => import(/* webpackChunkName: "new_vehicles" */ '@/views/NewVehicles.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/signup',
        name: 'signup',
        component: () => import(/* webpackChunkName: "signup" */ '@/views/SignUp.vue')
      },
      {
        path: '/signin',
        name: 'signin',
        component: () => import(/* webpackChunkName: "signin" */ '@/components/Signin.vue')
      },
      {
        path: '/profile',
        name: 'profile',
        component: () => import(/* webpackChunkName: "profile" */ '@/views/Profile.vue'),
        meta: {
          requiresAuth: true
        }
      },
    ],
    meta: {
      title: 'QRPass Admin'
    }

  },
  {
    path: '*',
    redirect: '/signin'
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// eslint-disable-next-line no-unused-vars
router.beforeEach((to, from, next) => {

  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  if(!nearestWithMeta) return next();

  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');
    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });
    tag.setAttribute('data-vue-router-controlled', '');
    return tag;
  })
  // Add the meta tags to the document head.
  .forEach(tag => document.head.appendChild(tag));


  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  console.log('requiresAuth: ' + requiresAuth)
  if (requiresAuth && !currentUser) {
    next('/signin');
  } else {
    next()
  }  
});

export default router
