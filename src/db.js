import firebase from 'firebase/app'
import 'firebase/firestore'
import firebaseAppConfig from './firebase-app-config.js'

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseAppConfig)
}
// Get a Firestore instance
export const db = firebase.firestore()
export const storage = firebase.storage()
// Export types that exists in Firestore
// This is not always necessary, but it's used in other examples
const { Timestamp, GeoPoint, FieldValue } = firebase.firestore
export { Timestamp, GeoPoint, FieldValue}