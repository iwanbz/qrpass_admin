import Vue from 'vue'
// @ts-ignore
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from './store'
import router from './router'
import '@mdi/font/css/materialdesignicons.css'
import firebase from 'firebase'
import 'firebase/firestore'
import { firestorePlugin } from 'vuefire'
const start = new Date().valueOf()
Vue.config.productionTip = false


// @ts-ignore
Vue.use(firestorePlugin)
let app = '';
firebase.auth().onAuthStateChanged(user => {
  console.log('firebase init took: ' + (new Date().valueOf() - start))
  if (!app) {
    // @ts-ignore
    app = new Vue({
      firestore () {
        return {
        }
      },
      vuetify,
      store,
      router,
      icons: {
        iconfont: 'mdi'//  'mdi'|| 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
      },
      render: h => h(App),
      created () {
        console.log('Vue Init: ' + (new Date().valueOf() - start))
        if (user) {
          this.$store.dispatch('reSignin', user)
        }
      }
    }).$mount('#app')    
  }
})
